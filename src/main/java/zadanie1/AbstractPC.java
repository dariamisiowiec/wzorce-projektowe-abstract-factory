package zadanie1;

public abstract class AbstractPC {
    String name;
    ComputerBrand brand;
    int cpuPower;
    double gpuPower;
    boolean isOverclocked;

    public AbstractPC(String name, ComputerBrand brand, int cpuPower, double gpuPower, boolean isOverclocked) {
        this.name = name;
        this.brand = brand;
        this.cpuPower = ((cpuPower > 100) ? 100 : ((cpuPower<0) ? 0 : cpuPower));
        this.gpuPower = ((gpuPower > 1) ? 1 : ((gpuPower<0) ? 0 :gpuPower));
        this.isOverclocked = isOverclocked;
    }

    @Override
    public String toString() {
        return "AbstractPC{" +
                "name='" + name + '\'' +
                ", brand=" + brand +
                ", cpuPower=" + cpuPower +
                ", gpuPower=" + gpuPower +
                ", isOverclocked=" + isOverclocked +
                '}';
    }
}
