package zadanie1;

public abstract class PCFactory {

    public static AppleMac createAppleMac1() {
        return new AppleMac("japko", 121, 1, false);
    }

    public static AppleMac createAppleMac2() {
        return new AppleMac ("inne japko", 2, 0.5, true);
    }

    public static AsusPC createAsusPG1(){
        return new AsusPC("nie japko", 3, 0.001, true);
    }

    public static AsusPC createAsusPG2(){
        return new AsusPC("też nie japko", 4, -2, true);
    }

    public static HpPC createHpPC1(){
        return new HpPC("gruszka", 7, 0.2, false);
    }

    public static HpPC createHpPC2() {
        return new HpPC ("inna gruszka", 3, 0.8, true);
    }

    public static SamsungPC createSamsungPC1(){
        return new SamsungPC("banan", 2, 1.13, false);
    }

    public static SamsungPC createSamsungPC2() {
        return new SamsungPC("inny banan", 1, 1, true);
    }
}
