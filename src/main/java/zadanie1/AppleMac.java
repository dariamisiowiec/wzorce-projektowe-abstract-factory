package zadanie1;

public class AppleMac extends AbstractPC {

    protected AppleMac(String name, int cpuPower, double gpuPower, boolean isOverclocked) {
        super(name, ComputerBrand.APPLE, cpuPower, gpuPower, isOverclocked);
    }
}
