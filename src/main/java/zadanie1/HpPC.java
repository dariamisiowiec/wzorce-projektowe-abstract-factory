package zadanie1;

public class HpPC extends AbstractPC {

    protected HpPC(String name, int cpuPower, double gpuPower, boolean isOverclocked) {
        super(name, ComputerBrand.HP, cpuPower, gpuPower, isOverclocked);
    }
}
