package zadanie1;

import java.util.LinkedList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<AbstractPC> listaPC = new LinkedList<AbstractPC>();
        listaPC.add(PCFactory.createAppleMac1());
        listaPC.add(PCFactory.createHpPC2());
        listaPC.add(PCFactory.createAsusPG2());

        for (AbstractPC pc : listaPC)
            System.out.println(pc);
    }
}
