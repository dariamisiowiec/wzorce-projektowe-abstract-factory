package zadanie1;

public class SamsungPC extends AbstractPC{

    protected SamsungPC(String name, int cpuPower, double gpuPower, boolean isOverclocked) {
        super(name, ComputerBrand.SAMSUNG, cpuPower, gpuPower, isOverclocked);
    }
}
