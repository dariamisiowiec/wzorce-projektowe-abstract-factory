package zadanie1;

public class AsusPC extends AbstractPC {

    protected AsusPC(String name, int cpuPower, double gpuPower, boolean isOverclocked) {
 //       super(name, cpuPower, gpuPower, isOverclocked);
 //       this.brand = ASUS;
        super(name, ComputerBrand.ASUS, cpuPower, gpuPower, isOverclocked);
    }
}
