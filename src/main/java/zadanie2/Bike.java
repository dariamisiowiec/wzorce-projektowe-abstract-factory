package zadanie2;

public class Bike {
    private MarkaRowera marka;
    private int iloscMiejsc;
    private int iloscPrzerzutek;
    private TypRowera typRowera;

    protected Bike(MarkaRowera marka, int iloscMiejsc, int iloscPrzerzutek, TypRowera typRowera) {
        this.marka = marka;
        this.iloscMiejsc = iloscMiejsc;
        this.iloscPrzerzutek = iloscPrzerzutek;
        this.typRowera = typRowera;
    }

    @Override
    public String toString() {
        return "Bike{" +
                "marka=" + marka +
                ", iloscMiejsc=" + iloscMiejsc +
                ", iloscPrzerzutek=" + iloscPrzerzutek +
                ", typRowera=" + typRowera +
                '}';
    }
}
