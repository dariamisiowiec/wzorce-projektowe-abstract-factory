package zadanie2;

public abstract class AbstractBikeFactory {

    public static Bike createKROSS(){
        return new Bike(MarkaRowera.KROSS, 1, 5, TypRowera.BICYCLE);
    }

    public static Bike createMERIDA(){
        return new Bike(MarkaRowera.MERIDA, 1, 6, TypRowera.BICYCLE);
    }

    public static Bike createINIANA() {
        return new Bike(MarkaRowera.INIANA, 2, 3, TypRowera.TANDEM);
    }

    public static Bike createFELT() {
        return new Bike(MarkaRowera.FELT, 1, 6, TypRowera.BICYCLE);
    }

    public static Bike createGOETZE() {
        return new Bike(MarkaRowera.GOETZE, 1, 1, TypRowera.BICYCLE);
    }
}
