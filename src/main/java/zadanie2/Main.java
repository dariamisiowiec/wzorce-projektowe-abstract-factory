package zadanie2;

import java.util.LinkedList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Bike> listaRowerow = new LinkedList<Bike>();
        listaRowerow.add(AbstractBikeFactory.createFELT());
        listaRowerow.add(AbstractBikeFactory.createGOETZE());
        listaRowerow.add(AbstractBikeFactory.createINIANA());
        listaRowerow.add(AbstractBikeFactory.createKROSS());
        listaRowerow.add(AbstractBikeFactory.createMERIDA());

        for (Bike bike : listaRowerow) {
            System.out.println(bike);
        }
    }
}
