package zadanie3.application;

import zadanie3.Person;

import java.time.LocalDateTime;
import java.util.LinkedList;


public abstract class AbstractApplicationFactory {

    public static Application createApplication;

    public static ConditionalStayApplication createConditionalStayApplication(Person osoba, LinkedList<Double> grades,
                                                                              String reason){
        return new ConditionalStayApplication(LocalDateTime.now(),
                "Gdańsk", osoba, "Chcę zostać na studiach", grades, reason);
    }

    public static SchoolarshipApplication createSchoolarshipApplication(Person osoba, LinkedList<Double> grades,
                                                                        LinkedList<String> extracurricular){
        return new SchoolarshipApplication(LocalDateTime.now(),
                "Gdańsk", osoba, "Chcę pieniędzy za dobre oceny", grades, extracurricular);
    }

    public static SemesterExtendApplication createSemesterExtendApplication(Person osoba, String reason){
        return new SemesterExtendApplication(LocalDateTime.now(), "Gdańsk", osoba,
                "Chcę dłużej studiować", reason);
    }

    public static SocialSchoolarshipApplication createSocialSchoolarshipApplication(Person osoba,
                                                                                    LinkedList<Double> grades,
                                                                                    Double income){
        return new SocialSchoolarshipApplication(LocalDateTime.now(),
                "Gdańsk", osoba,
                "Chcę pieniędzy z sociala", grades, income);
    }
}
