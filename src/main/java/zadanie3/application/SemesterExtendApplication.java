package zadanie3.application;

import zadanie3.Person;

import java.time.LocalDateTime;

public class SemesterExtendApplication extends Application {
    private String reason;

    public SemesterExtendApplication(LocalDateTime dataUtworzenia, String miejsceUtworzenia, Person daneAplikanta,
                                     String tresc, String reason) {
        super(dataUtworzenia, miejsceUtworzenia, daneAplikanta, tresc);
        this.reason = reason;
    }
}

