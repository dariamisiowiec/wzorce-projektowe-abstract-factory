package zadanie3.application;

import zadanie3.Person;

import java.time.LocalDateTime;
import java.util.LinkedList;

public class SchoolarshipApplication extends Application {
    private LinkedList<Double> grades;
    private LinkedList<String> extracurricularActivities;

    public SchoolarshipApplication(LocalDateTime dataUtworzenia, String miejsceUtworzenia, Person daneAplikanta,
                                   String tresc, LinkedList<Double> grades,
                                   LinkedList<String> extracurricularActivities) {
        super(dataUtworzenia, miejsceUtworzenia, daneAplikanta, tresc);
        this.grades = grades;
        this.extracurricularActivities = extracurricularActivities;
    }
}

