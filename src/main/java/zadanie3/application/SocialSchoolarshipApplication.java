package zadanie3.application;

import zadanie3.Person;

import java.time.LocalDateTime;
import java.util.LinkedList;

public class SocialSchoolarshipApplication extends Application {
    private LinkedList<Double> grades;
    private double totalFamilyIncome;

    public SocialSchoolarshipApplication(LocalDateTime dataUtworzenia, String miejsceUtworzenia, Person daneAplikanta,
                                         String tresc, LinkedList<Double> grades, double totalFamilyIncome) {
        super(dataUtworzenia, miejsceUtworzenia, daneAplikanta, tresc);
        this.grades = grades;
        this.totalFamilyIncome = totalFamilyIncome;
    }
}

