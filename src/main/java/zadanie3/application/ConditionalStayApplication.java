package zadanie3.application;

import zadanie3.Person;

import java.time.LocalDateTime;
import java.util.LinkedList;

public class ConditionalStayApplication extends Application {
    private LinkedList<Double> grades;
    private String reason;

    public ConditionalStayApplication(LocalDateTime dataUtworzenia, String miejsceUtworzenia, Person daneAplikanta,
                                      String tresc, LinkedList<Double> grades, String reason) {
        super(dataUtworzenia, miejsceUtworzenia, daneAplikanta, tresc);
        this.grades = grades;
        this.reason = reason;
    }
}
