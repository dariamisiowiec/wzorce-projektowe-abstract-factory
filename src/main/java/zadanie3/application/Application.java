package zadanie3.application;

import zadanie3.Person;

import java.time.LocalDateTime;

public class Application {
    protected LocalDateTime dataUtworzenia;
    protected String miejsceUtworzenia;
    protected Person daneAplikanta;
    protected String tresc;

    public Application(LocalDateTime dataUtworzenia, String miejsceUtworzenia, Person daneAplikanta, String tresc) {
        this.dataUtworzenia = dataUtworzenia;
        this.miejsceUtworzenia = miejsceUtworzenia;
        this.daneAplikanta = daneAplikanta;
        this.tresc = tresc;
    }
}
