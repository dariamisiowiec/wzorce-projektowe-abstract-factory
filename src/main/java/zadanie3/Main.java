package zadanie3;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Imię: ");
        String name = scanner.next();
        System.out.println("Nazwisko: ");
        String surname = scanner.next();
        System.out.println("Numer indeksu: ");
        String index = scanner.next();

        Person person = new Person(name, surname, index);
    }
}
